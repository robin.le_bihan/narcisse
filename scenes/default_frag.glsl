#version 450


layout(location = 0) out vec4 frag_colour;

in vec3 g_normal_vector;
in vec2 g_uv_vector;

uniform sampler2D texture_sampler;

void main () {
  frag_colour = vec4(0, 0, 0, 1);
}
